import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListItemComponent } from './list-item/list-item.component';
import { DetailComponent } from './detail/detail.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { MatCardModule } from '@angular/material/card';
//import { AuthGuardService } from './services/auth-gard.service';
//import { AuthService } from './services/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { LoginComponent } from './auth/components/login/login.component';
import { RegisterComponent } from './auth/components/register/register.component';
import {ResetPasswordComponent } from './auth/components/reset-password/reset-password.component'

 
 

@NgModule({
  declarations: [
    AppComponent,
    ListItemComponent,
    DetailComponent,
    LoginComponent,
    RegisterComponent,
    ResetPasswordComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatCardModule,
  ],
  exports:[   LoginComponent,
    RegisterComponent,
    ResetPasswordComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
