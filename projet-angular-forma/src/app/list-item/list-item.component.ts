import { Component, OnInit } from '@angular/core';
import { ListItemService } from '../services/list-item.Service'

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  initArray = [];
  constructor( private listItemservice: ListItemService) { }

  ngOnInit(){
    this.listItemservice.getPokemon().subscribe( list => {
     list.results.forEach(pokemon => {
       this.initArray.push({
         id: pokemon.id,
         name: pokemon.name  
        }    
        )
     });
    } );
  }

}
