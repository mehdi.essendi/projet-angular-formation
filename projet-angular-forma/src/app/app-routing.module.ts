import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListItemComponent } from './list-item/list-item.component';
import { DetailComponent } from './detail/detail.component';
import { LoginComponent } from './auth/components/login/login.component';
import { RegisterComponent } from './auth/components/register/register.component';
import { ResetPasswordComponent } from './auth/components/reset-password/reset-password.component';
//import { AuthGuardService as AuthGuard } from './services/auth-gard.service';

const routes: Routes = [{path: 'login', component: LoginComponent, outlet:'login'},
                        {path: 'register', component: RegisterComponent, outlet:'register'},
                        {path: 'reset-password', component: ResetPasswordComponent, outlet:'reset-password'},
                        {path: 'detail', component: DetailComponent, outlet:'detail'},
                        {path: 'list-item', component: ListItemComponent, outlet: 'list-item'},
                       ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
